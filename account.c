#include "account.h"

void acc_input(char *filename) {
	FILE *f = fopen(filename, "r");
	
	int i = 0;
	while(fscanf(f,"%s %s %d", acc_list[i].userID, acc_list[i].passWD, &acc_list[i].priority) != EOF) {
		i++;
	}
	
	acc_no = i;
}

void acc_display() {
	int i = 0;
	do {
		printf("\n%s\t%s\n", acc_list[i].userID, acc_list[i].passWD);
	} while (i++ < acc_no);
}

int check_valid_acc(Account acc, int *pri) {
	int i = 0;
	do {
		if (strcmp(acc.userID, acc_list[i].userID) == 0 &&
			strcmp(acc.passWD, acc_list[i].passWD) == 0) {
			*pri = acc_list[i].priority;
			return 1;
		}
	} while (i++ < acc_no);
	return 0;
}

int check_logining(Account acc) {
	FILE *f = fopen("logining.txt","r");
	int i = 0;
	Account acc_tmp;
	while (!feof(f)) {
		fscanf(f,"%s %s %d", acc_tmp.userID, acc_tmp.passWD, &acc_tmp.priority);
		if (strcmp(acc.userID, acc_tmp.userID) == 0)
			return 1;
	}
	return 0;
}
void login_in_file(Account acc) {
	FILE *f = fopen("logining.txt","r+");
	fseek(f, 0, SEEK_END);
	fprintf(f,"%s %s\n", acc.userID, acc.passWD);
	fclose(f);
}

void logout_in_file(Account acc) {
	FILE *f = fopen("logining.txt","r");

	Account acc_list_tmp[MAX_ACCOUNT];
	int i = 0;
	while (fscanf(f, "%s %s\n", acc_list_tmp[i].userID, acc_list_tmp[i].passWD) != EOF)
		i++;
    int line_no = i;

	fclose(f);

	f = fopen("logining.txt","w");	
	for(int i = 0; i < line_no; i++){
		if (strcmp(acc.userID, acc_list_tmp[i].userID) != 0 )
			fprintf(f,"%s %s\n", acc_list_tmp[i].userID, acc_list_tmp[i].passWD);
	}
    
	fclose(f);
}
