#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/wait.h>
#include <mysql.h>

#include "account.h"
#include "menu.h"
#include "worker.h"
#include "db.h"

#define BACKLOG 10   /* Number of allowed connections */
#define BUFF_SIZE 1024
#define HUGE_BUFF_SIZE 8192

void valid_argument(int argc, char *argv[], int *port);
void sig_chld(int signo);
void cli_process(int conn_sock);
void display_all_process(int conn_sock);
void login_process(int conn_sock, char *recv_data);
void add_process(int conn_sock, char *recv_data);
void search_process(int conn_sock, char *recv_data);
void edit_process(int conn_sock, char *recv_data);
void delete_process(int conn_sock, char *recv_data);
void db_handle();
void logout_process(char *recv_data);

Worker worker[MAX_WORKER_NUMBER];

int main(int argc, char *argv[])
{

	int port = 0;
	valid_argument(argc, argv, &port);

	acc_input(ACCOUNT_FILE);

	int listen_sock, conn_sock; 
	char recv_data[BUFF_SIZE];
	int bytes_sent, bytes_received;
	struct sockaddr_in server; 
	struct sockaddr_in client; 
	int sin_size;
	pid_t pid;
	
	// Construct a TCP socket to listen connection request
	if ((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  
		perror("\nError: ");
		return 0;
	}
	
	// Bind address to socket
	bzero(&server, sizeof(server));
	server.sin_family = AF_INET;         
	server.sin_port = htons(port);  
	server.sin_addr.s_addr = htonl(INADDR_ANY);  /* INADDR_ANY puts your IP address automatically */   
	if(bind(listen_sock, (struct sockaddr*)&server, sizeof(server))==-1){ 
		perror("\nError: ");
		return 0;
	}     
	
	// Listen request from client
	if(listen(listen_sock, BACKLOG) == -1){ 
		perror("\nError: ");
		return 0;
	}

	// handing zombie state
	signal(SIGCHLD,sig_chld);

	// Communicate with client
	while(1){
		//accept request
		sin_size = sizeof(struct sockaddr_in);
		if ((conn_sock = accept(listen_sock,( struct sockaddr *)&client, &sin_size)) == -1)
			if (errno == EINTR)
				continue;
			else
				perror("\nError: ");			
		
		if ((pid = fork()) == 0) {
			close(listen_sock);
			printf("You got a connection from %s\n", inet_ntoa(client.sin_addr) ); 
			/* handle information from client */
			cli_process(conn_sock);
			exit(0);
		}		
		close(conn_sock);	
	}

	close(listen_sock);
	return 0;
}

void valid_argument(int argc, char *argv[], int *port) {
	if (argc == 2) {
		int i;
		char *port_str = argv[1];
		for (i = 0; port_str[i] != '\0'; i++) {
			if (!isdigit(port_str[i])) {
				printf("Port is invalid\n");
				exit(0);
			}
		}
		if (port_str[i] == '\0') *port = atoi(port_str);
	} else {
		printf("ERROR!!! Syntax like: ./server port\n");
		exit(0);
	}
}

void cli_process(int conn_sock) {
	char recv_data[BUFF_SIZE],code[BUFF_SIZE];
	int bytes_sent, bytes_received;
	Account acc;
	
	//start conversation
	while(1){
		//receives message from client
		bytes_received = recv(conn_sock, recv_data, BUFF_SIZE-1, 0); 
		if (bytes_received <= 0){
			printf("\nConnection closed");
			break;
		} else{
			recv_data[bytes_received] = '\0';
			printf("Receive: %s\n", recv_data);
		}

		if (!strcmp(recv_data,"quit"))
			break;
		
		// handout recevied data
		sscanf(recv_data,"%[^|]", code);
		
		if (strcmp(code, "login") == 0)  // login
			login_process(conn_sock,recv_data);
		else if (strcmp(code, "displayall") == 0)  // display all
			display_all_process(conn_sock);
		else if (strcmp(code, "add") == 0)  // add
			add_process(conn_sock,recv_data);
		else if (strcmp(code,"search") == 0) //search
			search_process(conn_sock,recv_data);
		else if (strcmp(code,"edit") == 0)  //edit
	    	edit_process(conn_sock,recv_data);
		else if (strcmp(code,"delete") == 0)  //delete
			delete_process(conn_sock,recv_data);
		else if (strcmp(code,"logout") == 0)  //logout
			logout_process(recv_data);
		
	} //end conversation
}

void login_process(int conn_sock, char *recv_data) {
	char result[BUFF_SIZE], pri_str[BUFF_SIZE], code[BUFF_SIZE];
	int bytes_sent,pri;
	Account acc;
	
	sscanf(recv_data,"%[^|]|%[^|]|%[^|]", code,  acc.userID, acc.passWD);
				
	if (check_valid_acc(acc,&pri) && !check_logining(acc)) {
		sprintf(pri_str,"%d",pri);
		strcpy(result,"success|");
		strcat(result,pri_str);
		login_in_file(acc);
	}
	else
		strcpy(result,"login failed");
	
	if ((bytes_sent = send(conn_sock, result, strlen(result), 0)) <= 0){
		printf("\nConnection closed");
		return;
	}
}

void display_all_process(int conn_sock) {
	db_handle();

	int bytes_sent, count = 0;
	char str[HUGE_BUFF_SIZE];
	memset(str, '\0', (strlen(str)+1));
			
	while (count < worker_no) {
		strcat(str,worker[count].id);
		strcat(str,"|");
		strcat(str,worker[count].name);
		strcat(str,"|");
		strcat(str,worker[count].position);
		strcat(str,"|");
		strcat(str,worker[count].sex);
		strcat(str,"|");
		strcat(str,worker[count].address);
		strcat(str,"|");
		strcat(str,worker[count].birthday);
		strcat(str,"#");
							
		count++;
	}

	if ((bytes_sent = send(conn_sock, str, strlen(str), 0)) <= 0){
		printf("\nConnection closed");
		return;
	}
}

void add_process(int conn_sock, char *recv_data) {
	connect_database();

	int bytes_sent;
	char code[BUFF_SIZE], query[BUFF_SIZE], result[BUFF_SIZE];
	Worker w;
	sscanf(recv_data,"%[^|]|%[^|]|%[^|]|%[^|]|%[^|]|%[^|]", code, w.name, 
		w.position,w.sex,w.address,w.birthday);
		
	sprintf(query,"INSERT INTO %s(name,position,sex,address,birthday) \
		VALUE ('%s','%s','%s','%s','%s')", DATABASE_TABLE,w.name, 				\
		w.position,w.sex,w.address,w.birthday);

	memset(result, '\0', strlen(result) + 1);
	if (!mysql_query(conn, query)) 
		strcat(result, "ADD SUCCESS");
	else
		strcat(result, "ADD FAILED");
		
	bytes_sent = send(conn_sock, result, strlen(result), 0); 
	if (bytes_sent <= 0){
		printf("\nConnection closed");
		return;
	}
}

void search_process(int conn_sock, char *recv_data) {
	connect_database();

	MYSQL_RES *res;
	MYSQL_ROW row;
	char query[BUFF_SIZE], code[BUFF_SIZE], str[HUGE_BUFF_SIZE];
	int i = 0, count = 0, bytes_sent;
	Worker w_list[MAX_WORKER_NUMBER];
	Worker w;
	
	sscanf(recv_data,"%[^|]|%[^|]", code, w.name);
	
	sprintf(query,"SELECT * FROM %s WHERE name = '%s' ", DATABASE_TABLE, w.name) ;
	mysql_query(conn, query);
	res = mysql_use_result(conn);
	
	while ((row = mysql_fetch_row(res)) != NULL) {
		strcpy(w_list[i].id, row[0]);
		strcpy(w_list[i].name, row[1]);
		strcpy(w_list[i].position, row[2]);
		strcpy(w_list[i].sex, row[3]);
		strcpy(w_list[i].address, row[4]);
		strcpy(w_list[i].birthday, row[5]);
		i++;
	}

	memset(str, '\0', (strlen(str)+1));
	
	if (i == 0) {
		strcat(str,"search_no_result");
	} else {
		while (count < i) {
			strcat(str, w_list[count].id);
			strcat(str,"|");
			strcat(str, w_list[count].name);
			strcat(str,"|");
			strcat(str, w_list[count].position);
			strcat(str,"|");
			strcat(str, w_list[count].sex);
			strcat(str,"|");
			strcat(str, w_list[count].address);
			strcat(str,"|");
			strcat(str, w_list[count].birthday);
			strcat(str,"#");
			
			count++;
		}
	}
		
	if ((bytes_sent = send(conn_sock, str, strlen(str), 0)) <= 0){
		printf("\nConnection closed");
		return;
	}
}

void edit_process(int conn_sock, char *recv_data) {
	char buff[BUFF_SIZE], query[BUFF_SIZE], code[BUFF_SIZE];
	int id,bytes_sent,bytes_received;
	Worker w;

	sscanf(recv_data,"%[^|]|%d", code, &id);
				
	char result[BUFF_SIZE] = "edit|ok";
	bytes_sent = send(conn_sock, result, strlen(result), 0); 
	if (bytes_sent <= 0){
		printf("\nConnection closed");
		return;
	}

	bytes_received = recv(conn_sock, buff, BUFF_SIZE-1, 0);
	buff[bytes_received] = '\0';
	sscanf(buff,"%[^|]|%[^|]|%[^|]|%[^|]|%[^|]", w.name, w.position,w.sex,w.address,w.birthday);

	connect_database();
	sprintf(query,"UPDATE %s SET name = '%s', position = '%s', sex = '%s', \
		address = '%s', birthday = '%s' WHERE id = %d ",											\
		DATABASE_TABLE, w.name, w.position,w.sex,w.address,w.birthday, id);

	char result1[BUFF_SIZE];
	memset(result1, '\0', strlen(result1) + 1);
	if (!mysql_query(conn, query)) 
		strcat(result1, "EDIT SUCCESS");
	else
		strcat(result1, "EDIT FAILED");
	 
	bytes_sent = send(conn_sock, result1, strlen(result1), 0); 
	if (bytes_sent <= 0){
		printf("\nConnection closed");
		return;
	}
}

void delete_process(int conn_sock, char *recv_data) {
	connect_database();
	
	char code [BUFF_SIZE], query[BUFF_SIZE];
	int id;
	
	sscanf(recv_data,"%[^|]|%d", code, &id);
	sprintf(query,"DELETE FROM %s WHERE id = %d ", DATABASE_TABLE, id) ;
	mysql_query(conn, query);
}

void logout_process(char *recv_data) {
	char code[BUFF_SIZE];
	Account acc;
	sscanf(recv_data,"%[^|]|%[^|]|%s", code,  acc.userID, acc.passWD );
	logout_in_file(acc);
}

void db_handle() {
	connect_database();	
	MYSQL_RES *res;
	MYSQL_ROW row;
	char query[BUFF_SIZE];
	
	sprintf(query,"SELECT * FROM %s", DATABASE_TABLE) ;
	
	if (mysql_query(conn, query)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	
	res = mysql_use_result(conn);
	
	int i = 0;
	while ((row = mysql_fetch_row(res)) != NULL) {
   	strcpy(worker[i].id, row[0]);
   	strcpy(worker[i].name, row[1]);
		strcpy(worker[i].position, row[2]);
		strcpy(worker[i].sex, row[3]);
		strcpy(worker[i].address, row[4]);
		strcpy(worker[i].birthday, row[5]);
		i++;
	}
	worker_no = i;
}

void sig_chld(int signo){
	pid_t pid;
	int stat;

	while((pid=waitpid(-1,&stat,WNOHANG))>0)
		printf("\n[ForkingServer] Child %d terminated\n",pid);
	return;
}
