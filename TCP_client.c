#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <termios.h>

#include "account.h"
#include "menu.h"
#include "worker.h"


#define BUFF_SIZE  2048
#define HUGE_BUFF_SIZE 8192
#define BASE_LENGTH	256

void valid_arguments (int argc, char *argv[], char *server_ip, int *server_port);
void client_body(int client_sock);
void program(int client_sock);
void login(int client_sock, char *code, int *pri, Account *acc);
void logout(int client_sock, Account acc);
void add(int client_sock);
void edit(int client_sock);
void search(int client_sock, char *name, int *search_number);
void delete(int client_sock);
void display_all(int client_sock);

int main(int argc, char *argv[]){
	int server_port = 0;
	char server_ip[16] = "";
	valid_arguments(argc, argv,&server_ip, &server_port);

	int client_sock;
	struct sockaddr_in server_addr;
	
	// Construct socket
	client_sock = socket(AF_INET,SOCK_STREAM,0);
	
	// Specify server address
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(server_port);
	server_addr.sin_addr.s_addr = inet_addr(server_ip);
	
	// Request to connect server
	if(connect(client_sock, (struct sockaddr*)&server_addr, sizeof(struct sockaddr)) < 0){
		printf("\nError!Can not connect to sever! Client exit imediately!\n");
		return 0;
	}
	
	// Communicate with server		   
	client_body(client_sock);
	
	close(client_sock);
	return 0;
}

int check_str(char *str) {
  int i;
  for(i=0; i < strlen(str);i++ ){
    if((!isalpha(str[i])) && (!isdigit(str[i])) && (str[i]!= '_') && (str[i]!= '-')) 
    	return 0;
  }
  return 1;
}

void valid_arguments (int argc, char *argv[], char *server_ip, int *server_port) {
	if (argc == 3) {

		struct sockaddr_in tmp_addr;
		if (inet_pton(AF_INET, argv[1], &(tmp_addr.sin_addr)) == 0) {
			printf("IP Address is invalid\n!");
			exit(0);
		} else {
			strcpy(server_ip, argv[1]);
		}

		int i;
		char *port_str = argv[2];
		for (i = 0; port_str[i] != '\0'; i++) {
			if (!isdigit(port_str[i])) {
				printf("Port is invalid\n!");
				exit(0);
			}
		}
		if (port_str[i] == '\0') *server_port = atoi(port_str);
	} else {
		printf("ERROR!!! Syntax like: ./client ip port\n");
		exit(0);
	}
}

void client_body(int client_sock) {
	while (1) {
		menu_home();
		int bytes_sent;
		char c[10];
		memset(c, '\0', strlen(c) + 1);
		
		__fpurge(stdin);
		scanf("%s", c);		
		if (strlen(c) != 1)
			continue;
		switch (c[0]) {
		case '1':
			program(client_sock);
			break;
		case '2':
			bytes_sent = send(client_sock, "quit", strlen("quit"), 0);
			close(client_sock);
			return;
		default:
			printf("\nInvalid!\n");
		}
	}
}

void program(int client_sock) {
	while(1){
		Account acc;
		int pri;
		char code[BUFF_SIZE];
		
		login(client_sock, code, &pri, &acc);
		
		if (strcmp(code, "success") == 0) {
			if (pri == 1) {
				while (1) {
					menu_admin();
					
					char buff1[BUFF_SIZE], name[BUFF_SIZE];
					char choice[10] ;
					int search_number;

					memset(choice, '\0', strlen(choice) + 1);
					__fpurge(stdin);
					scanf("%s",choice);
					if (strlen(choice) != 1)
						continue;

					switch (choice[0])  {
					case '1': // display all
						display_all(client_sock);
						break;
					case '2': // add worker
						add(client_sock);
						break;
					case '3': // edit
						edit(client_sock);
						break;
					case '4': //delete
						delete(client_sock);
						break;
					case '5': //search
						printf("\n\n-----------SEARCH----------\n\n");						
						printf("Enter name to search : ");
						scanf("%s",name);
						search(client_sock, name, &search_number);
						break;
					case '6':
						logout(client_sock,acc);
						return;
					default:
						printf("\nInvalid!\n");
					} // end of switch 
				} // end of while login success
			} // account is admin
			else {
				while (1) {
					menu_user();

					int search_number, check_times = 0;
					char choice[10];
					char name[BUFF_SIZE];

					memset(choice, '\0', strlen(choice) + 1);
					__fpurge(stdin);
					scanf("%s",choice);
					if (strlen(choice) != 1)
						continue;
					
					switch (choice[0]) {
					case '1':
						display_all(client_sock);
						break;
					case '2':
						printf("\n\n-----------SEARCH----------\n\n");		
						printf("Enter name to search : ");
						scanf("%s",name);
						search(client_sock, name, &search_number);
						break;
					case '3':
						logout(client_sock,acc);
						return;
					default:
						printf("\nInvalid\n\n");
					}  
				}
			} //account is user
		} // end of if login success
		else {
			printf("\n\nLOGIN FAILED!\n\n");
			return;
		} // if login fail
	}
}

void login(int client_sock, char *code, int *pri, Account *acc) {
	int bytes_sent, bytes_received;
	char buff[BUFF_SIZE];
		
	menu_login();

	int check_times_login = 0;
	
	while (1) {
		if (check_times_login == 5) {
			printf("\n\nInput is wrong too much times!\n\n");
			exit(-1);
		}
		__fpurge(stdin);
		printf("\nUsername: ");
		memset(acc->userID,'\0',(strlen(acc->userID)+1));
		fscanf(stdin,"%s",acc->userID);
		if (check_str(acc->userID) == 1) {
			check_times_login = 0;
			break;
		}
		printf("\n\nInvalid USERNAME. Retype!\nUSERNAME includes characters, numbers, _ and -\n");
		check_times_login++;
	}
	
	
	while (1) {
		if (check_times_login == 5) {
			printf("\n\nInput is wrong too much times!\n\n");
			exit(-1);
		}
		__fpurge(stdin);
		
		// ignore signals 
		signal(SIGINT, SIG_IGN);
		signal(SIGTERM, SIG_IGN);
		// no echo
		struct termios term;
		tcgetattr(1, &term);
		term.c_lflag &= ~ECHO;
		tcsetattr(1, TCSANOW, &term);

		printf("\nPassword: ");
		memset(acc->passWD,'\0',(strlen(acc->passWD)+1));
		fscanf(stdin,"%s",acc->passWD);	
		//reset term
		term.c_lflag |= ECHO;
		tcsetattr(1, TCSANOW, &term);

		if (check_str(acc->passWD) == 1) {
			check_times_login = 0;
			break;
		}
		printf("\n\nInvalid PASSWORD. Retype!\n");
		check_times_login++;
	}
	
	
	memset(buff,'\0',(strlen(buff)+1));
	strcpy(buff,"login|");
	strcat(buff,acc->userID);
	strcat(buff,"|");
	strcat(buff,acc->passWD);
	
	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}
	
	bytes_received = recv(client_sock, buff, BUFF_SIZE-1, 0);
	if(bytes_received <= 0){
		printf("\nError!Cannot receive data from sever!\n");
		return;
	}
	
	buff[bytes_received] = '\0';
	sscanf(buff,"%[^|]|%d", code, pri);	
}

void add(int client_sock) {
	int bytes_sent,bytes_received;
	char buff[BUFF_SIZE];
	Worker w;

	printf("\n\n-----------ADD----------\n\n");
	printf("Enter name: ");
	scanf("%s",w.name);
	printf("Enter position (M or E): ");
	scanf("%s",w.position);
	printf("Enter sex (M or F): ");
	scanf("%s",w.sex);
	printf("Enter address: ");
	__fpurge(stdin);
	scanf("%[^\n]%*c",w.address);
	printf("Enter birthday: ");
	scanf("%s",w.birthday);

	memset(buff,'\0',(strlen(buff)+1));
	strcat(buff,"add|");
	strcat(buff,w.name);
	strcat(buff,"|");
	strcat(buff,w.position);
	strcat(buff, "|");
	strcat(buff, w.sex);
	strcat(buff, "|");
	strcat(buff, w.address);
	strcat(buff, "|");
	strcat(buff, w.birthday);

							
	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}
							
	bytes_received = recv(client_sock, buff, BUFF_SIZE-1, 0);
	buff[bytes_received] = '\0';
	printf("\n\n%s\n\n", buff);
}

void edit(int client_sock) {
	char name[BUFF_SIZE], id_str[BUFF_SIZE];
	int bytes_sent, bytes_received, search_number;
	Worker w;

	printf("\n\n-----------EDIT----------\n\n");
	printf("Enter name to edit: ");
	scanf("%s", name);

	search(client_sock, name, &search_number);
	if (search_number == 0) {
		printf("\n\nNO DATA SUITABLE!\n\n");
		return;
	}

	printf("\n\nInput ID to edit (0 to reject): ");
	scanf("%s",id_str);
	if (strcmp(id_str,"0") == 0)
		return;
		
	char buff[BUFF_SIZE] = "edit|";
	strcat(buff,id_str);
							
	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}
							
	bytes_received = recv(client_sock, buff, BUFF_SIZE-1, 0);
	buff[bytes_received] = '\0';
							
	if (strcmp(buff, "edit|ok") == 0) {
		printf("\n\nNEW INFO\n\n");
		printf("Enter new name: ");
		scanf("%s",w.name);
		printf("Enter new position (M or E): ");
		scanf("%s",w.position);
		printf("Enter new sex (M or F): ");
		scanf("%s",w.sex);
		printf("Enter new address: ");
		__fpurge(stdin);
		scanf("%[^\n]%*c",w.address);
		printf("Enter new birthday (XX/YY/ZZZZ): ");
		__fpurge(stdin);
		scanf("%s",w.birthday);
		
		memset(buff,'\0',(strlen(buff)+1));
		strcat(buff, w.name);
		strcat(buff, "|");
		strcat(buff, w.position);
		strcat(buff, "|");
		strcat(buff, w.sex);
		strcat(buff, "|");
		strcat(buff, w.address);
		strcat(buff, "|");
		strcat(buff, w.birthday);

		bytes_sent = send(client_sock, buff, strlen(buff), 0);
		if(bytes_sent <= 0){
			printf("\nConnection closed!\n");
			return;
		}
		
		bytes_received = recv(client_sock, buff, BUFF_SIZE-1, 0);
		buff[bytes_received] = '\0';
		printf("\n\n%s\n\n", buff);
	} 
}

void search(int client_sock, char *name, int *search_number) {
	char str[HUGE_BUFF_SIZE], buff[BUFF_SIZE];
	int bytes_sent, bytes_received;
	Worker w;
	
	*search_number = 0;
	
	memset(buff, '\0', (strlen(buff)+1));
	memset(str, '\0', (strlen(str)+1));
	
	strcat(buff, "search|");
	strcat(buff, name);
							
	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}
							
	bytes_received = recv(client_sock, str, HUGE_BUFF_SIZE-1, 0);
	str[bytes_received] = '\0';
	if (strcmp(str,"search_no_result") == 0) {
		printf("\n\nNo result!\n\n");
		return;
	}
	
	printf("\n\n%-5s\t%-10s\t%-5s\t%-5s\t%-10s\t%-10s\n\n","ID","NAME","POS","SEX","ADDRESS","BIRTHDAY");

	char *p;
	p = strtok(str,"#");
	while (p != NULL) {
		(*search_number)++;
		sscanf(p,"%[^|]|%[^|]|%[^|]|%[^|]|%[^|]|%[^|]",w.id, w.name, w.position,w.sex,w.address,w.birthday);
		printf("%-5s\t%-10s\t%-5s\t%-5s\t%-10s\t%-10s\n",w.id, w.name, w.position,w.sex,w.address,w.birthday);		
		p = strtok(NULL,"#");
	}		
}

void delete(int client_sock) {
	int search_number,id,bytes_sent;
	char id_str[BUFF_SIZE],name[BUFF_SIZE];
	printf("\n\n-----------DELETE----------\n\n");
							
	printf("Enter name to delete: ");
	scanf("%s", name);

	search(client_sock, name, &search_number);

	if (search_number == 0)
		return;

	printf("\n\nInput ID to delete (0 to reject): ");
	scanf("%s",id_str);
	if (strcmp(id_str,"0") == 0)
		return;
							
	char buff[BUFF_SIZE] = "delete|";
	strcat(buff,id_str);

	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}

	printf("\n\nDELETE SUCCESS\n\n");
}

void display_all(int client_sock) {
	int bytes_sent,bytes_received;
	char str[HUGE_BUFF_SIZE];
	char buff[BUFF_SIZE];
	Worker w;

	memset(buff, '\0', (strlen(buff)+1));
	strcpy(buff,"displayall|");
	bytes_sent = send(client_sock, buff, strlen(buff), 0);
	if(bytes_sent <= 0){
		printf("\nConnection closed!\n");
		return;
	}
						
	memset(str, '\0', (strlen(str)+1));
	bytes_received = recv(client_sock, str, HUGE_BUFF_SIZE-1, 0);
	str[bytes_received] = '\0';

	printf("%-5s\t%-10s\t%-5s\t%-5s\t%-10s\t%-10s\n","ID","NAME","POS","SEX","ADDRESS","BIRTHDAY");

	char *p;
	p = strtok(str,"#");
	while (p != NULL) {
		sscanf(p,"%[^|]|%[^|]|%[^|]|%[^|]|%[^|]|%[^|]",w.id, w.name, w.position,w.sex,w.address,w.birthday);
		printf("%-5s\t%-10s\t%-5s\t%-5s\t%-10s\t%-10s\n",w.id, w.name, w.position,w.sex,w.address,w.birthday);
		p = strtok(NULL,"#");
	}						
}

void logout(int client_sock, Account acc) {
	char buff[BUFF_SIZE];
	int bytes_sent;
	memset(buff, '\0', (strlen(buff)+1));
	strcpy(buff,"logout|");
	strcat(buff, acc.userID);
	strcat(buff,"|");
	strcat(buff, acc.passWD);
	
	if((bytes_sent = send(client_sock, buff, strlen(buff), 0)) <= 0){
		printf("\nConnection closed!\n");
		return;
	}
}
