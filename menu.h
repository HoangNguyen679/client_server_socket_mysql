#ifndef MENU_H
#define MENU_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void menu_home();
void menu_login();
void menu_admin();
void menu_user();
	
#endif
