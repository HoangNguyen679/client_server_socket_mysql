CC=gcc
CFLAGS=-w -std=c99 

PROGS = server client

all: ${PROGS}

server: TCP_server.c account.c menu.c worker.c db.c
	$(CC) $(CFLAGS) -o $@ $^ `mysql_config --cflags --libs`

client: TCP_client.c account.c menu.c worker.c 
	$(CC) $(CFLAGS) -o $@ $^ 

clean: *
	rm -f ${PROGS}
