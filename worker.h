#ifndef WORKER_H
#define WORKER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_WORKER_NUMBER 30
#define MAX_WORKER_ID 11
#define MAX_WORKER_NAME 30
#define MAX_WORKER_POS 10
#define MAX_WORKER_SEX 10
#define MAX_WORKER_ADDR 30
#define MAX_WORKER_BIR 15

struct Worker_{
	char id[MAX_WORKER_ID];
	char name[MAX_WORKER_NAME];
	char position[MAX_WORKER_POS];
	char sex[MAX_WORKER_SEX];
	char address[MAX_WORKER_ADDR];
	char birthday[MAX_WORKER_BIR];
};

typedef struct Worker_ Worker;

int worker_no;

Worker add_worker();

#endif
