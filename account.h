#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_ACCOUNT 10
#define MAX_USER_LEN 40
#define MAX_PASS_LEN 40

#define ACCOUNT_FILE "account.txt"
#define LOGINING_FILE "logining.txt"

struct Account_{
  char userID[MAX_USER_LEN];
  char passWD[MAX_PASS_LEN];
  int priority;
};

typedef struct Account_ Account;

Account acc_list[MAX_ACCOUNT];
int acc_no;

void acc_input(char *filename);
void acc_display();
int check_valid_acc(Account acc, int *pri);
int check_logining(Account acc);
void login_in_file(Account acc);
void logout_in_file(Account acc);

#endif
