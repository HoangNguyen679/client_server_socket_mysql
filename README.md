# Client - Server through Socket with MySQL  #

This sample give a client - server communication to get data in MySQL.

## Getting Started


### Prerequisites

* C Compiler, Make
* MySQL

### Installing

Run make file
```
make
```
Run server
```
./server port_number
```
Run client
```
./client server_ip port_number
```
### Enjoy

## Authors

* **Hoang Nguyen** 
* **Viet Nguyen** 