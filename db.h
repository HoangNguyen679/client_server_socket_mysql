#ifndef DATABASE_H
#define DATABASE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mysql.h>

#define SERVER "localhost"
#define USER "root"
#define PASSWORD "123456"
#define DATABASE "db"
#define DATABASE_TABLE "db3"

MYSQL *conn;

void connect_database();

#endif
